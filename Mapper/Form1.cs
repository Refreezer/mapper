﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mapper
{
    public partial class Form1 : Form
    {
        public static List<string> mapInfo = new List<string>();
        private List<PictureBox> boxes = new List<PictureBox>();
        private Point lastClick;
        private Rectangle lastRectangle;
        private string fileName;

        private Pen pen = new Pen(Brushes.Black, 0.8f)
        {
            DashStyle = System.Drawing.Drawing2D.DashStyle.Dash
        };

        public Form1()
        {
            InitializeComponent();
            pictureBox.Enabled = false;
            saveButton.Enabled = false;
            cancelButton.Enabled = false;
            clearButton.Enabled = false;
        }

        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clear_Click(sender, e);
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.DefaultExt = "bmp";

            DialogResult result = ofd.ShowDialog();

            if (result != DialogResult.Cancel)
            {
                try
                {
                    pictureBox.Image = new Bitmap(ofd.FileName);
                    saveButton.Enabled = true;
                    pictureBox.Enabled = true;
                    cancelButton.Enabled = true;
                    clearButton.Enabled = true;
                    Invalidate();
                    pictureBox.Paint += Paint;
                    fileName = ofd.FileName;
                }
                catch
                {
                    MessageBox.Show("Not an image", "error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void aboutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //TODO -- информация о приложении
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //TODO -- инструкция
        }

        private void pictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            lastClick = e.Location;
            pictureBox.Paint -= Paint;
            pictureBox.Paint += Selection;
        }

        void Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(Pens.Black, lastRectangle);
            e.Graphics.FillRectangle(Brushes.Transparent, lastRectangle);
            e.Dispose();
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            foreach (var box in boxes)
            {
                box.Dispose();
            }

            mapInfo.Clear();
            boxes.Clear();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            if (boxes.Count < 1) return;
            if (mapInfo.Count != 0)
            {
                mapInfo.RemoveAt(mapInfo.Count - 1);
            }

            boxes[boxes.Count - 1].Dispose();
            boxes.RemoveAt(boxes.Count - 1);
        }

        private void Selection(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(pen, lastRectangle);
            e.Graphics.FillRectangle(Brushes.Transparent, lastRectangle);
            e.Dispose();
        }

        private void pictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            pictureBox.Paint -= Selection;
            pictureBox.Paint += Paint;
            pictureBox.Invalidate();
            PictureBox box = new PictureBox();
            box.Parent = pictureBox;
            box.Location = lastRectangle.Location;
            box.Size = lastRectangle.Size;
            boxes.Add(box);
            Form urlInput = new URLForm();
            urlInput.ShowDialog();
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            int dx = e.Location.X - lastClick.X;
            int dy = e.Location.Y - lastClick.Y;
            Size sizeOfRectangle = new Size(Math.Abs(dx), Math.Abs(dy));
            Rectangle rectangle = new Rectangle();
            if (dx >= 0 & dy >= 0)
            {
                rectangle = new Rectangle(lastClick, sizeOfRectangle);
            }

            if (dx < 0 & dy > 0)
            {
                rectangle = new Rectangle(e.Location.X, lastClick.Y, sizeOfRectangle.Width, sizeOfRectangle.Height);
            }

            if (dx < 0 & dy < 0)
            {
                rectangle = new Rectangle(e.Location, sizeOfRectangle);
            }

            if (dx > 0 & dy < 0)
            {
                rectangle = new Rectangle(lastClick.X, e.Location.Y, sizeOfRectangle.Width, sizeOfRectangle.Height);
            }

            lastRectangle = rectangle;
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                (sender as PictureBox).Refresh();
        }

        private void buildMap()
        {
            string s = $"<html>\n<body>\n<img src = \" {fileName} \"width=\"750\" height=\"420\" usemap=" +
                       "\"#mapname\" border =\"0\">\n<map name = \"mapname\">";

            int i = 0;
            foreach (var info in mapInfo)
            {
                var tmp = info.Split();
                var b = boxes[i];
                i++;
                string position = b.Location.X + "," +
                                  b.Location.Y + "," +
                                  (b.Location.X + b.Width) + "," +
                                  (b.Location.Y + b.Height);
                s += $"\n<area shape = \"rect\" coords = \" {position} \" href = \"{tmp[1]} \" alt= \"" +
                     tmp[0] + "\">";
            }

            s += "\n</map>\n</body>\n</html>";
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.DefaultExt = "html";
            sfd.FileName = "map";
            if (sfd.ShowDialog() != DialogResult.Cancel)
                using (StreamWriter file = new StreamWriter(sfd.FileName))
                {
                    file.Write(s);
                }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            buildMap();
        }
    }
}